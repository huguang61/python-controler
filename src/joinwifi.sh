#!/bin/sh

[ "$1" = "2G" -o "$1" = "5G" ] || {
	echo "Usage: $0 <2G|5G> <ssid> <password>
eg:
	$0 2G OpenWrt_B1300_2G_TEST goodlife"
	exit 1
}

[ "$1" = "2G" ] && IFACE=wifi0 && STAIF=ath0
[ "$1" = "5G" ] && IFACE=wifi1 && STAIF=ath1

SSID=$2
PASS=$3

uci set wireless.@wifi-iface[0].disabled=1
uci set wireless.@wifi-iface[1].disabled=1

uci set wireless.sta=wifi-iface
uci set wireless.sta.network='wwan'
uci set wireless.sta.mode='sta'
uci set wireless.sta.key=$PASS
uci set wireless.sta.encryption='psk-mixed'
uci set wireless.sta.device=$IFACE
uci set wireless.sta.ifname=$STAIF
uci set wireless.sta.ssid=$SSID

uci set network.wwan=interface
uci set network.wwan.proto='static'
uci set network.wwan.ipaddr='192.168.8.3'
uci set network.wwan.netmask='255.255.255.0'
uci set network.wwan.ifname=$STAIF
uci set network.wwan.metric='20'

uci set firewall.@zone[1].network='wan wan6 wwan'
uci set firewall.@zone[1].input='ACCEPT'
uci set firewall.@zone[1].forward='ACCEPT'

uci commit network
uci commit wireless
uci commit firewall

# /etc/init.d/network reload
# /etc/init.d/firewall reload
/etc/init.d/network restart
