#!/usr/bin/env python
# -*- coding: utf-8 -*-


import paramiko
import time
import socket
paramiko.util.log_to_file("paramiko.log")

count_to_power_port = {
    1: 1,
    2: 15,
    3: 2,
    4: 16,
    5: 3,
    6: 17,
    7: 4,
    8: 18,
    9: 5,
    10: 19,
    11: 6,
    12: 20,
    13: 7,
    14: 21,
    15: 8,
    16: 22,
    17: 9,
    18: 23,
    19: 10,
    20: 24,
    21: 11,
    22: 25,
    23: 12,
    24: 26,
    25: 13,
    26: 27,
    27: 14,
    28: 28,
}

class SSHClient():
    def __init__(self, host, port=22, username="root", password='talkingdt'):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        self.recv_buff = 1024*1024*1024

    def connect(self):
        self.ssh.connect(hostname=self.host, port=self.port, username=self.username, password=self.password)
        self.chan = self.ssh.invoke_shell()
        time.sleep(0.1)
        self.banner = self.chan.recv(self.recv_buff)

    def get_current_ssh_config(self):
        config = {}
        config["host"] = self.host
        config["port"] = self.port
        config["username"] = self.username
        config["password"] = self.password
        return config

    def get_banner(self):
        return self.banner
    def recv_timeout(self, size=1024*1024*1024, timeout=2):
        self.chan.setblocking(0)
        total_data=[];data='';begin=time.time()
        while 1:
            #if you got some data, then break after wait sec
            if total_data and time.time()-begin>timeout:
                break
            #if you got no data at all, wait a little longer
            elif time.time()-begin>timeout*2:
                break
            try:
                data=self.chan.recv(size)
                if data:
                    total_data.append(data)
                    begin=time.time()
                else:
                    time.sleep(0.1)
            except:
                pass
        return ''.join(total_data)
    def recv(self, size=1024*1024*1024):
        return self.chan.recv(size)

    def recv_expect(self, expect, timeout=5):
        buff = ""
        while expect not in buff:
            resp = ""
            try:
                resp = self.chan.recv(self.recv_buff)
            except socket.timeout, e:
                print "error ", e
                return False
            print "resp is ", str([resp])
            buff += resp
            time.sleep(0.3)
            timeout -= 1
            if timeout == 0:
                print str([expect])
                print " \nis not in : \n"
                print str([buff])
                print 20*"-"
                return False
        return buff

    def send(self, cmd, timeout=1):
        self.chan.send(cmd + "\n")
        time.sleep(timeout)
        return self.chan.recv(self.recv_buff)

    def send_only(self, cmd, timeout=1):
        self.chan.send(cmd + "\n")
        time.sleep(timeout)

    def close(self):
        self.ssh.close()

    def send_expect(self, cmd, expect, timeout=5):
        print "------send_expect init--------"
        self.chan.send(cmd + "\n")
        print "--------chan.send(cmd)-----------"
        buff = ""
        while expect not in buff:
            resp = ""
            print "the timeout is: ", timeout
            try:
                print "-----try------"
                resp = self.recv_timeout(self.recv_buff)
                print "-------chan.recv()------"
            except socket.timeout, e:
                print "socket.timeout"
                return False
            buff += resp
            print "the buf is：", resp
            time.sleep(0.5)
            timeout -= 1
            if timeout == 0:
                print "expect ", expect + " \nis not in : \n" + buff + "\n buff over"
                return False
        return buff

