#!/usr/bin/env python
# -*- coding: utf-8 -*-
from selenium import webdriver
import time
import os
import time
import commands
import getpass
import requests
from datetime import datetime
import re
import json
import logging
from head import SSHClient, count_to_power_port
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import telnetlib

host = '192.168.8.1'
user = 'root'
pwd = "talkingdt"

# 写入配置信息


def write_config_json(bow):
    manufacturer_name = bow.find_element_by_id(
        "manufacturer_name").get_attribute("value")
    customer_name = bow.find_element_by_id(
        "customer_name").get_attribute("value")
    with open("/python-web/html/config.json", "r") as f:
        data = json.load(f)
    data[0]["manufacturer_name"] = manufacturer_name
    data[0]["customer_name"] = customer_name
    with open("/python-web/html/config.json", "w") as f:
        json.dump(data, f)
# 获取设备类型
def get_test_device_type(bow):
    pass_route = bow.find_element_by_id("select-route").get_attribute("value")
    if pass_route == "":
        return False
    js = '$(\"#device_type\").text(\"%s\")' % pass_route
    bow.execute_script(js)
    return pass_route
# 获取code值


def get_code_of_rou(id, status_id, bow):
    code = bow.find_element_by_id(id).get_attribute("value")
    if len(code) != 24 and len(code) != 42 and len(code) != 17:
        # code  qj00001 6ae6ef0823d24925
        js = '$("#%s").text("长度不对")' % status_id
        bow.execute_script(js)
        time.sleep(1)
        return False
    return code
# 第一步和第二步获取设备的ip地址


def get_ip(device_type, bow):
    if "GL-AR150" == device_type:
        device_ip = "192.168.1.1"
        return device_ip
    if "GL-B1300" == device_type:
        device_ip = "192.168.1.1"
        return device_ip
    elif "GL-AR300M" == device_type:
        device_ip = "192.168.1.1"
        return device_ip
    elif "GL-AR750" == device_type:
        device_ip = '192.168.1.1'
        return device_ip
    elif "GL-MT300N-V2" == device_type:
        device_ip = "10.10.10.254"
        return device_ip
    else:
        return False
# 启动浏览器


def start_firefox(html, os_type):
    if "Window" in os_type:
        bow = webdriver.Firefox()
        bow.get("file:///d:/python-web/view/" + html + ".html")
    else:
        capabilities = webdriver.DesiredCapabilities().FIREFOX
        capabilities["marionette"] = False
        binary = FirefoxBinary(r'/usr/bin/firefox')
        bow = webdriver.Firefox(firefox_binary=binary,
                                capabilities=capabilities)
        bow.get("file:///python-web/view/" + html + ".html")
    bow.maximize_window()
    return bow
# 等待设备启动可以ping通

def wait_ping_ip_start(os_type, ip="192.168.1.1"):
    ress = []
    res = ""
    if "Window" in os_type:
        cmd = 'ping ' + ip + ' -w 2000 -n 2'
    else:
        cmd = 'ping ' + ip + " -c 2 -W 2"
    while True:
        print "ping ----", ip, res
        res = os.popen(cmd).read()
        if "TTL" in res or 'ttl' in res:
            return True
        else:
            return False
        ress.append(res)
        time.sleep(0.3)
# 通过ssh去ping通，判断是否中继上


def wait_ping_ip_by_ssh(ssh, ip, ssid, os_type, time_out=60):
    if "Window" in os_type:
        cmd = 'ping ' + ip + ' -w 2000 -n 2'
    else:
        cmd = 'ping ' + ip + " -c 2 -W 2"
    after = datetime.now()
    while True:
        res = ssh.send(cmd, timeout=2)
        if "ttl" in res or "TTL" in res:
            ssh.send_only('\x03')
            ssh.close()
            print "set ssid is {} ok, ping ok".format(ssid)
            return True
        else:
            time_out -= 1
            print "wait wifi is restart ", res
            time.sleep(1)
            if time_out == 0:
                print "%s second ping error" % (datetime.now() - after)
                ssh.close()
                return False
            continue
# 通过telnet连接设备


def connect_telnet_host(host="192.168.1.1", timeout=5):
    try:
        tn = telnetlib.Telnet()
        tn.open(host, timeout=2)
        banner = tn.read_until("/#", timeout=2)
        print "the banner is: ", banner
    except Exception, e:
        print e
        return False
    time.sleep(0.2)
    return tn


def connect_device_by_ssh(h="192.168.8.1", p="goodlife", timeout=5):
    try:
        ssh = SSHClient(host=h, port=22, username="root", password=p)
        ssh.connect()
    except Exception, e:
        ssh.close()
        print e
        return False
    return ssh
# 连接设备


def connect_device(os_type, device_ip, bow, id="default"):
    res_ping = wait_ping_ip_start(os_type, device_ip)
    if res_ping is False:
        print "%s:stop" % device_ip
        js = '''$(\"#button-%s\").text(\"正在ping设备，等待几秒!\")''' % id
        bow.execute_script(js)
        return False
    print "连接中。。。。。。。", datetime.now()
    tn = connect_telnet_host(host=device_ip)
    if tn is False:
        js = '''$(\"#button-%s\").text(\"请连接路由器等待几秒!\")''' % id
        bow.execute_script(js)
        return False
    print "已连接正在测试中", datetime.now()
    js = "document.getElementById(\"button-%s\").setAttribute(\"class\", \"btn btn-lg btn-success\");" % id
    bow.execute_script(js)
    js = '''$(\"#button-%s\").text(\"已连接路由器!\")''' % id
    bow.execute_script(js)
    return tn
# 等待设备下线


def wait_ping_ip_down(os_type, ip="192.168.1.1"):
    ress = []
    res = ""
    if "Window" in os_type:
        cmd = 'ping ' + ip + ' -w 2000 -n 2'
    else:
        cmd = 'ping ' + ip + " -c 2 -W 2"
    while True:
        print "ping ----", ip, res
        res = os.popen(cmd).read()
        if "TTL" not in res and 'ttl' not in res:
            return True
        ress.append(res)
        time.sleep(0.3)
# mt-300n-v2校准检测


def mt_300n_v2_calibration(tn, bow):
    tn.write(
        "echo $(hexdump /dev/mtd3 -s $((0x58)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')" + "\n")
    time.sleep(.3)
    v1 = re.findall(r'27', tn.read_very_eager())
    tn.write(
        "echo $(hexdump /dev/mtd3 -s $((0x5e)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')" + "\n")
    time.sleep(.3)
    v2 = re.findall(r'27', tn.read_very_eager())
    if len(v1) and len(v2):
        print "设备没有校准"
        js = "document.getElementById(\"button-default\").setAttribute(\"class\", \"btn btn-lg btn-danger\");"
        bow.execute_script(js)
        js = "$(\"#button-default\").text(\"请先校准设备!\");"
        bow.execute_script(js)
        return False
    else:
        print "设备已经校准"
        return True
# 校准检测


def calibration(tn, bow):
    print "--------检查校验状态----------------"
    tn.write("echo $(hexdump /dev/mtdblock6 -s $((0x108f)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')" + "\n")
    time.sleep(.3)
    v1 = str(tn.read_very_eager()[89:]).replace("\r\n", " ").split(" ")
    print v1
    print v1[1]
    tn.write("echo $(hexdump /dev/mtdblock6 -s $((0x1095)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')" + "\n")
    time.sleep(.3)
    v2 = str(tn.read_very_eager()[87:]).replace("\r\n", " ").split(" ")
    print v2
    print v2[1]
    tn.write("echo $(hexdump /dev/mtdblock6 -s $((0x109b)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')" + "\n")
    time.sleep(.3)
    v3 = str(tn.read_very_eager()[87:]).replace("\r\n", " ").split(" ")
    print v3
    print v3[1]
    print "-----------------------------------"
    if len(v1[1]) and len(v2[1]) and len(v3[1]):
        print "设备已经校验"
        return True
    else:
        print "设备未校验"
        js = "document.getElementById(\"button-default\").setAttribute(\"class\", \"btn btn-lg btn-danger\");"
        bow.execute_script(js)
        js = "$(\"#button-default\").text(\"请先校准设备!\");"
        bow.execute_script(js)
        return False


def firststore(tn, bow):
    print "------测试内存--------"
    tn.write("echo $(free -h |grep Mem | awk -F ' ' '{print $2}')" + "\n")
    time.sleep(.3)
    v1 = str(tn.read_very_eager()).replace("\r\n", " ").split(" ")[-3]
    print "内存为", v1
    if int(v1) > 40000:
        return True
    else:
        js = "document.getElementById(\"button-default\").setAttribute(\"class\", \"btn btn-lg btn-danger\");"
        bow.execute_script(js)
        js = '''$(\"#button-default\").text(\"设备内存过小!\")'''
        bow.execute_script(js)
        return False
# 重置开关测试


def test_reset_tel(tn, browser, type, time_out=10):
    js = 'document.getElementById("button-reset").style.display=""'
    browser.execute_script(js)
    if "MT300N-V2" in type:
        resp = "reset key is pressed\r\n"
    else:
        resp = "reset key is pressed\r\nreset key is pressed\r\n"
    while time_out:
        res = tn.read_until(resp, timeout=5)
        if resp in res:
            js = '$(\"#button-reset\").text(\"重置按钮测试完成\")'
            browser.execute_script(js)
            print "reset ", res
            return True
        else:
            time.sleep(1)
            time_out -= 1
            js = '$(\"#button-reset\").text(\"请按重置按钮,还剩%s次，退出本次测试\")' % time_out
            browser.execute_script(js)
            print "超时次数：", time_out
    return False

# led测试


def test_led_tel(tn, browser, type):
    js = 'document.getElementById("button-led-on").style.display=""'
    browser.execute_script(js)
    time_out = 0.3
    if "MT300N-V2" in type:
        time_out = 1
    tn.write("killall led_wlan_lan_blink 2>/dev/null\n")
    tn.write("led on\n")
    time.sleep(time_out)
    tn.write("led off\n")
    time.sleep(time_out)
    tn.write("led on\n")
    time.sleep(time_out)
    tn.write("led off\n")
    time.sleep(time_out)
    tn.write("led on\n")
    time.sleep(time_out)
    tn.write("led off\n")
    time.sleep(time_out)
    tn.write("led on\n")
    time.sleep(time_out)
    tn.write("led off\n")
    time.sleep(time_out)
    tn.write("led on\n")
    time.sleep(time_out)
    tn.write("/etc/rc.local\n")
    res = tn.read_until("led indicator", timeout=2)
    print res
    time.sleep(0.5)
    js = '''$(\"#button-led-on\").text(\"LED 测试完成\")'''
    browser.execute_script(js)
    return True

# 拨动开关测试


def test_switch_tel(tn, browser, type, time_out=5):
    print "beging switch test"
    js = 'document.getElementById("button-test").style.display=""'
    browser.execute_script(js)
    dsc = 'switch moved to middle\r\nswitch moved to left\r\nswitch moved to middle\r\nswitch moved to right\r\n'
    while time_out:
        res = tn.read_until(dsc, timeout=5)
        if dsc in res:
            print "success"
            js = '$(\"#button-test\").text(\"拨动开关测试完成\")'
            browser.execute_script(js)
            return True
        else:
            time.sleep(1)
            time_out -= 1
            js = '$(\"#button-test\").text(\"测试失败，请拨动开关至少两个来回,还剩%s次，退出本次测试\")' % time_out
            browser.execute_script(js)
            print "测试开关中，超时次数：", time_out
    return False

# mt300n-v2拨动开关测试


def test_switch_tel_mt300N_v2(tn, browser, type, time_out=5):
    print "beging test_switch_tel_mt300N_v2 "
    js = 'document.getElementById("button-test").style.display=""'
    browser.execute_script(js)
    dsc = ['switch moved to middle',
           'switch moved to left', 'switch moved to right']
    while time_out:
        res = tn.read_until('switch moved to middle', timeout=5)
        if 'switch moved to middle' in res:
            res = tn.read_until('switch moved to left', timeout=5)
            if 'switch moved to left' in res:
                res = tn.read_until('switch moved to right', timeout=5)
                if 'switch moved to right' in res:
                    print "success"
                    js = '$(\"#button-test\").text(\"拨动开关测试完成\")'
                    browser.execute_script(js)
                    return True
        else:
            time.sleep(1)
            time_out -= 1
            js = '$(\"#button-test\").text(\"测试失败，请拨动开关至少两个来回,还剩%s次，退出本次测试\")' % time_out
            browser.execute_script(js)
            print "测试300n-v2的开关中，剩余超时次数", time_out
    return False
# usb模块测试


def test_USP_tel(tn, browser, time_out=6):
    js = 'document.getElementById("button-usb").style.display=""'
    browser.execute_script(js)
    while time_out:
        res = tn.read_until("find udisk", timeout=10)
        if "find udisk" in res:
            js = '$(\"#button-usb\").text(\"USB已插入，测试完成\")'
            browser.execute_script(js)
            time.sleep(1)
            return True
        else:
            time.sleep(1)
            time_out -= 1
            js = '$(\"#button-usb\").text(\"USB测试失败，请重复插拔USB设备,还剩%s次，退出本次测试\")' % time_out
            browser.execute_script(js)
            print "测试usb中，超时次数：", time_out
    return False
# 测试sd卡和3g模块


def test_sd_3g(tn, browser):
    print "begin sd 3g"
    js = '$("#button-test").text("测试SD卡中...")'
    browser.execute_script(js)
    js = 'document.getElementById("button-test").style.display=""'
    browser.execute_script(js)
    time.sleep(1)
    while True:
        tn.write("check_sdcard\n")
        res = tn.read_until("sdcard ok", timeout=1)
        if "sdcard ok" in res:
            js = '$("#button-test").text("SD卡OK，测试3G模块中...")'
            browser.execute_script(js)
            break
        else:
            print res
            js = '$("#button-test").text("SD卡测试失败，再测试一遍")'
            browser.execute_script(js)
            time.sleep(1)
            continue
    js = '$("#button-test").text("测试3G模块中...")'
    browser.execute_script(js)
    time.sleep(1)
    while True:
        tn.write("check_ttyUSB\n")
        res = tn.read_until("ttyUSB ok", timeout=1)
        if "ttyUSB ok" in res:
            js = '$("#button-test").text("3G模块OK")'
            browser.execute_script(js)
            break
        else:
            print res
            js = '$("#button-test").text("3G模块测试失败，再测试一遍")'
            browser.execute_script(js)
            time.sleep(1)
            continue
# 分离扫码兼容各种情况


def split_code(code):
    codeList = code.split()
    if len(codeList) == 3:
        if ":" in codeList[0]:  # E4:95:6E:42:14:18 da21418 15777267fa5b8c35
            if len(codeList[1]) > len(codeList[2]):
                return codeList[2], codeList[1], codeList[0], 3
            else:
                return codeList[1], codeList[2], codeList[0], 3
        elif ":" in codeList[2]:  # da21418 15777267fa5b8c35 E4:95:6E:42:14:18
            if len(codeList[0]) > len(codeList[1]):
                return codeList[1], codeList[0], codeList[2], 3
            else:
                return codeList[0], codeList[1], codeList[2], 3
        elif ":" in codeList[1]:
            if len(codeList[0]) > len(codeList[2]):
                return codeList[2], codeList[0], codeList[1], 3
            else:
                return codeList[0], codeList[2], codeList[1], 3
    elif len(codeList) == 2:
        return codeList[0], codeList[1], 2
    elif len(codeList) == 1:
        return codeList[0], 1

# 从code tuple中提取关键信息


def get_two_par_from_code_tuple(code_tuple):
    code_len = code_tuple[-1]
    if code_len == 2 or code_len == 3:
        return code_tuple[0]
    elif code_len == 1:
        return get_ddns_code_from_mac(code_tuple[0])
    return False
# 从mac地址中获取ddns等信息


def get_ddns_code_from_mac(mac):
    payload = {'action':'mac2ddns','mac_address':mac}
    r = requests.get("http://192.168.16.17/api.php", params=payload)
    return r.json()['ddns_name']
# 第二次 测试300n-v2 开关


def switch_miwifi_in_shell_mt300n_v2(tn, browser, time_out=5):
    print "beging switch test"
    js = 'document.getElementById("button-test").style.display=""'
    browser.execute_script(js)
    dsc = ['switch moved to middle', 'switch moved to left']
    while time_out:
        res = tn.read_until(dsc[0], timeout=20)
        if dsc[0] in res:
            res = tn.read_until(dsc[1], timeout=20)
            if dsc[1] in res:
                print "success"
                js = '$(\"#button-test\").text(\"拨动开关测试完成\")'
                browser.execute_script(js)
                return True
        else:
            time.sleep(1)
            time_out -= 1
            js = '$(\"#button-test\").text(\"测试失败，请拨动开关至少两个来回,还剩%s次，退出本次测试\")' % time_out
            browser.execute_script(js)
            print "次数：", time_out
    return False
# 测试300m


def switch_miwifi_in_shell_300m(tn, browser, time_out=5):
    print "beging 300m test"
    js = 'document.getElementById("button-test").style.display=""'
    browser.execute_script(js)
    dsc = ['switch moved to middle', 'switch moved to left']
    while time_out:
        res = tn.read_until(dsc[0], timeout=20)
        if dsc[0] in res:
            res = tn.read_until(dsc[1], timeout=20)
            if dsc[1] in res:
                print "success"
                js = '$(\"#button-test\").text(\"拨动开关测试完成\")'
                browser.execute_script(js)
                return True
        else:
            time.sleep(1)
            time_out -= 1
            js = '$(\"#button-test\").text(\"测试失败，请拨动开关至少两个来回,还剩%s次，退出本次测试\")' % time_out
            browser.execute_script(js)
            print "次数：", time_out
    return False
# 测试拨动开关


def swith_for_ar750(tn, browser,time_out=5):
    print 'bing ar750 test'
    js = 'document.getElementById("button-test").style.display=""'
    browser.execute_script(js)
    dsc = ['switch moved to middle', 'switch moved to right']
    while time_out:
        res = tn.read_until(dsc[0], timeout=20)
        if dsc[0] in res:
            res = tn.read_until(dsc[1], timeout=20)
            if dsc[1] in res:
                print "success"
                js = '$(\"#button-test\").text(\"拨动开关测试完成\")'
                browser.execute_script(js)
                return True
        else:
            time.sleep(1)
            time_out -= 1
            js = '$(\"#button-test\").text(\"测试失败，请拨动开关至少两个来回,还剩%s次，退出本次测试\")' % time_out
            browser.execute_script(js)
            print "次数：", time_out
    return False


def switch_miwifi_in_shell(tn, browser, time_out=5):
    print "beging switch test"
    js = 'document.getElementById("button-test").style.display=""'
    browser.execute_script(js)
    dsc = 'switch moved to middle\r\nswitch moved to right\r\n'
    while time_out:
        res = tn.read_until(dsc, timeout=20)
        if dsc in res:
            print "success"
            js = '$(\"#button-test\").text(\"拨动开关测试完成\")'
            browser.execute_script(js)
            return True
        else:
            time.sleep(1)
            time_out -= 1
            js = '$(\"#button-test\").text(\"测试失败，请拨动开关至少两个来回,还剩%s次，退出本次测试\")' % time_out
            browser.execute_script(js)
            print "次数：", time_out
    return False
