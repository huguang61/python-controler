#!/usr/bin/env python
#encoding: utf-8

import logging
import time
import sys
import getopt
import requests
import random
import string
import demjson
import socket, fcntl, struct

from paho.mqtt.client import Client as Mqtt

VERSION = "1.0.1"

"""
    1.每个设备在没有绑定之前，都是处于暴露在公共服务状态，只有
    在控制端与其建立了通信连接的时候，这时候，设备和终端处于
    通信独享状态。
    2.
"""
KIRA_BROADCAST_RECEIVER = 'kira_inet'         # 广播接受者

logging.basicConfig(level=logging.DEBUG,
    format = '[%(asctime)s] %(levelname)s %(message)s',
    datefmt = '%Y-%m-%d %H:%M:%S')
root = logging.getLogger()
root.setLevel(logging.NOTSET)


class Action(object):
    """行为动作类封装"""
    def __init__(self, receiver, sender, actionName, payload=None):
        self.decode(payload)                 # 在实例化这个对象时，首先做的就是对data数据进行decode操作
        self.receiver = receiver
        self.sender = sender
        self.action = actionName

    def decode(self, payload):
        if payload:
            try:
                json = demjson.decode(payload)          # 将data数据解码为json字符串
                self.__dict__.update(json)              # 为创建的实例对象增加含有data数据的实例属性
            except demjson.JSONDecodeError:
                raise ValueError

    def encode(self):
        return demjson.encode(self.__dict__).encode("utf-8")          # 将实力属性再次转化为json类型

    @staticmethod
    def buildAction(receiver, sender, actionName, payload=None): # 这里也是将发送者变为接收者，将接收者变为发送者
        return Action(receiver, sender, actionName, payload)   # 这里返回的是一个action对象，

    @staticmethod
    def buildReplyAction(action, actionName, payload=None):       # 获取到action对象，以及其对应的蓝图名字
        if action:        # 再次调用一个函数，传入的参数是获取到的对象的参数
            return Action.buildAction(action.sender, action.receiver, actionName, payload)

class MqttClient(object):
    """Mqtt通讯封装"""
    # client = KiraClient(("192.168.16.17", 1883), 'gliot')
    def __init__(self, address):
        if not isinstance(address, tuple) or len(address) != 2:         # 地址必须是元组形式，有ip地址，同时有端口
            raise ValueError("Invalid address.")
        def on_connect(client, userdata, flags, rc):
            self.handleConnected()                                        # 这个是和服务器之间来建立联系的

        def on_message(client, userdata, msg):
            self.handleMessage(msg.topic, msg.payload)                    # 这个是用来接受服务端的信息的

        self.client = Mqtt()                                             # 这个是获取客户端的实例对象，
        self.address = address
        self.client.on_connect = on_connect                               # 为客户端的实例对象创建了两个实例对象
        self.client.on_message = on_message

    def handleConnected(self):
        logging.info("MqttClient.handleConnected()")

    def handleMessage(self, topic, payload):
        logging.info("MqttClient.handleMessage() topic={} payload={}".format(topic,payload))

    def publish(self, topic, payload=None, qos=2, retain=False):
        logging.info("MqttClient.publish() topic={} payload={}".format(topic,payload))
        self.client.publish(topic, payload, qos, retain)

    def subscribe(self, topic, qos=0):
        logging.info("MqttClient.subscribe() topic={}".format(topic))
        self.client.subscribe(topic, qos)

    def sendMessage(self, topic, payload=None, qos=2, retain=False):
        logging.info("MqttClient.sendMessage() topic={}".format(topic))
        self.client.publish(topic, payload, qos, retain)           # 发送信息给服务器

    def start(self):
        self.client.connect_async(self.address[0], self.address[1], keepalive=13*60*60)      # 这个函数表示和客户端相连，连接时间为13个小时
        self.client.loop_start()          # 这个仅仅是为了守护这个进程

    def stop(self):
        logging.info("MqttClient.stop()")
        self.client.loop_stop()

    def username_pw_set(self, username, password=None):
        self.client.username_pw_set(username, password)

    def will_set(self, topic, payload=None, qos=2, retain=False):
        logging.info("MqttClient.will_set() topic={} payload={}".format(topic, payload))
        self.client.will_set(topic, payload, qos, retain)      # 客户端断开但是没有通知，服务器会代为通知其没有断开

class KiraBase(MqttClient):
    """
    Kira  物联通信基类
    nodeid 节点号:如果用户有传便是用户所定制的，如果没有传，在可以调用ip地址的情况下为ip地址，如果取不到便是随机码
    accountid 通道号: 如果用户有传便是用户定制，如果没有传，便是默认的kiraiot
    """

    def __init__(self, address, accountid=None, nodeid=None, authkey=None, username=None, password=None):
        """username和password是mqtt账号密码"""
        logging.info("KiraBase.__init__() address=({}, {}), accountid={}, nodeid={}".format(address[0], address[1], accountid, nodeid))
        # 首先将信息写入日志文件，接着调用父类来初始化，创建了客户端的实例对象，同时也创建了一个mqtt实例对象
        super(KiraBase, self).__init__(address)
        self.username = username
        self.password = password
        if accountid:
            self.accountid = accountid          #
        else:
            self.accountid = u'kiraiot'        # 如果没有传就是直接写成kiraiot
        if nodeid:
            self.nodeid = nodeid
        else:
            try:
                self.nodeid = self.get_ip2('eth0')      # 这个是服务器的ip地址，
            except:
                self.nodeid = self.activation_code(random.randint(1, 225))      # 这个是随机码，这样能够唯一断定是某个设备发过来的信息就好了
        self.authkey = authkey
        self.handlers = {}

    def get_ip2(self, ifname):     # 使用socket套接字来建立联系，这里基本上是没有使用，这个是为了拿到IP
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])

    def activation_code(self, id, length=10):
        """
        id + L + 随机码
        string模块中的3个函数：string.letters，string.printable，string.printable
        """
        prefix = hex(int(id))[2:]+ 'L'
        length = length - len(prefix)
        chars=string.ascii_letters+string.digits
        return prefix + ''.join([random.choice(chars) for i in range(length)])

    def sendAction(self, action):
        logging.info("KiraBase.sendAction()")
        if action:
            topic = "{self.accountid}/{action.receiver}/{self.nodeid}/{action.action}".format(self=self, action=action)
            payload = action.encode()
            self.sendMessage(topic, payload)            # 发送信息

    def handleAction(self, action):
        logging.info("YqmiotClient.handleAction()")
        if action:
            handler = self.handlers.get(action.action)
            if handler:
                handler(self, action)

    def handleConnected(self):
        logging.info("KiraBase.handleConnected()")
        super(KiraBase, self).handleConnected()
        topic = "{self.accountid}/{self.nodeid}/#".format(self=self)
        self.subscribe(topic)

    def handleMessage(self, topic, payload):
        logging.info("KiraBase.handleMessage() topic={} payload={}".format(topic, payload))
        super(KiraBase, self).handleMessage(topic, payload)
        try:
            account,receiver,sender,command = topic.split("/")
            account = str(account)
            receiver = str(receiver)
            sender = str(sender)
        except:
            logging.error("Invalid topic. {}".format(topic))
            return

        action = Action.buildAction(receiver, sender, command)

        if action:
            try:
                action.decode(payload)
            except ValueError:
                logging.info("the payload format invalid. {} {}".format(topic, payload))
                return

            try:
                self.handleAction(action)
            except:
                raise
        else:
            logging.info("the action not found. {}".format(topic))

    def route(self, actionName):      # 这个装饰器就是通过这样的一个方法来实现将路由与函数一一对应的，估计flask里面也是采用的这个模式的
        logging.info("YqmiotClient.route() actionName=%s" % actionName)
        def decorator(func):
            self.handlers[actionName] = func
            return func
        return decorator

class KiraClient(KiraBase):
    """
        Kira物联客户端
    """
    # 这里是函数的入口，路由器的实例对象就是在这里创建的
    def start(self):
        # 离线通知，就是在创建路由器的时候就已经为其死亡写好遗言了。
        topic = "{}/{}/{}/{}".format(self.accountid, KIRA_BROADCAST_RECEIVER, self.nodeid, 'offline')      # 这个是主题，这里面有数据的标题，包含有各种信息
        payload = {"id": self.nodeid, "action": 'offline'}         # 这个是消息的内容
        self.will_set(topic, demjson.encode(payload))                # 客户端断开但是没有通知，服务器会代为通知其没有断开

        super(KiraClient, self).start()                              # 这个函数表示开启连接

    def handleConnected(self):
        super(KiraClient, self).handleConnected()
        logging.info("Connect server successfully.")

        # 上线通知
        actionOnline = Action.buildAction(KIRA_BROADCAST_RECEIVER, self.nodeid, "online")
        self.sendAction(actionOnline)












