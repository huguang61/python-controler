#!/bin/bash
#!/bin/bash
# Use at own risk ;-)
# Author: aminkira2018@gmail.com
usage() {
cat << EOF
Configure wifi on boot partition of SD card for Raspberry PI.
Usage: configureWifi -s <SSID> -p <password>
Parameters:
  -s: SSID of your wifi
  -p: password of your wifi
EOF
exit 0
}

configureWIFI() {
echo "Starting WIFI setup..."
echo '# /etc/wpa_supplicant/wpa_supplicant.conf
# 这句是什么意思
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
# 网络设置
network={
ssid="'$WIFI_SSID'"
psk="'$WIFI_PASSWORD'"
proto=RSN
scan_ssid=0
key_mgmt=WPA-PSK
pairwise=CCMP
auth_alg=OPEN
priority=10
}' > $root/wpa_supplicant/wpa_supplicant.conf
sudo service networking restart
sudo service dhcpcd restart
sleep 3
echo "Done..."
}

root=/etc
argumentCnt=0
WIFI_SSID=$1
WIFI_PASSWORD="goodlife"
sudo service networking restart
sudo service dhcpcd restart
sleep 3
time_out=12

while (($time_out > 0)); do
time_out=`expr $time_out - 1`;
if sudo iwlist wlan0 scanning | grep $WIFI_SSID;then
    sudo chmod 777 $root/wpa_supplicant/wpa_supplicant.conf
    configureWIFI
    sudo dhclient wlan0
    break;
fi
sleep 2
echo "not exist ssid"
done
if (($time_out != 0));then
  echo "have connected"
else
  echo "not exist"
fi
exit 0
