#!/bin/bash
sudo apt-get update
sudo apt-get install raspberrypi-kernel-headers                           #安装之后安装网卡需要的linux内核包
sudo apt-get install telnet                                               # 树莓派不自带telnet命令，需要手动安装
#sudo apt-get install iperf                                               # 树莓派不自带iperf命令，需要手动安装
sudo apt-get install iperf3
sudo apt-get install iceweasel                                            # 安装firefox浏览器
sudo apt-get install nginx                                                # 提供静态文件服务，主要用于被测设备来下载iperf文件，进行吞吐量测试
sudo apt-get install mysql-server                                         # 用于autoip.sh文件，自动上报ip地址

sudo /etc/init.d/nginx restart                                            # 重启nginx服务
sudo cp ~/python-controler/setup/src/iperf_linux /var/www/html/iperf_linux       # 将iperf文件部署到www下，以供被测试设备下载使用
sudo cp ~/python-controler/setup/src/iperf_mt7620 /var/www/html/iperf_mt7620
sudo cp ~/python-controler/setup/src/iperf_mt7628 /var/www/html/iperf_mt7628
sudo cp ~/python-controler/setup/src/iperf_ar750 /var/www/html/iperf_ar750
sudo cp ~/python-controler/setup/src/iperf_b1300 /var/www/html/iperf_b1300
sudo cp ~/python-controler/setup/src/install-wifi /usr/bin/install-wifi          # 开始安装网卡驱动
sudo chmod +x /usr/bin/install-wifi
sudo install-wifi
sudo cp ~/python-controler/setup/src/8812au.conf /etc/modprobe.d/8812au.conf     # 禁用网卡的电源管理
sudo systemctl enable dhcpcd.service                                            # 开启dhcpcd服务开机自启动
sudo service networking restart                                                 # 重启网卡服务
sudo service dhcpcd restart
sudo cp ~/python-controler/setup/src/config.txt /boot/config.txt                 # 禁用蓝牙和自带网卡配置
sudo cp ~/python-controler/setup/src/ctl_client /usr/bin/ctl_client              # 移植用于控制电源板的客户端
sudo chmod +x /usr/bin/ctl_client
sudo cp ~/python-controler/setup/src/ssid.sh /usr/bin/ssid.sh                    # 移植树莓派连接wifi的脚本
sudo chmod +x /usr/bin/ssid.sh
sudo cp ~/python-controler/setup/src/ssh.sh /usr/bin/ssh.sh                      # 移植树莓派使用的ssh的脚本
sudo chmod +x /usr/bin/ssh.sh
sudo cp ~/python-controler/setup/src/telnet.sh /usr/bin/telnet.sh                # 移植树莓派使用的telnet的脚本
sudo chmod +x /usr/bin/telnet.sh
sudo cp ~/python-controler/setup/src/autoip.sh /usr/bin/autoip.sh                # 移植树莓派自动上传ip的脚本
sudo chmod +x /usr/bin/autoip.sh
sudo mv /etc/network/interfaces /etc/network/interfaces.back                    # 配置网络信息
sudo cp ~/python-controler/setup/src/interfaces /etc/network/interfaces

cd ~/python-controler/setup/src/demjson/
sudo python setup.py install                                                      # 6.这个包已经拉下来了
cd ~/python-controler/setup/src/paho.mqtt.python/                                  # 5.这个包已经拉下来了
sudo python setup.py install
# 兼容旧方式
#sudo pip install -U pip -i https://pypi.douban.com/simple                        # 7.这个包已经来下来了
cd ~/python-controler/setup/src/pip/
sudo python setup.py install
#sudo pip install selenium -i https://pypi.douban.com/simple                      # 1.这个已经拉下来了
cd ~/python-controler/setup/src/selenium-python/
sudo python setup.py install
sudo apt-get install build-essential libssl-dev libffi-dev python-dev
#sudo pip install cffi --upgrade -i https://pypi.douban.com/simple
cd ~/python-controler/setup/src/cffi/
sudo python setup.py install                                                      # 2.这个包已经拉下来了
#sudo pip install cryptography --upgrade -i https://pypi.douban.com/simple        # 3.这个包已经拉下来了
cd ~/python-controler/setup/src/cryptography
sudo python setup.py install
#sudo pip install paramiko -i https://pypi.douban.com/simple
cd ~/python-controler/setup/src/paramiko/                                          # 4.这个包已经拉下来了
sudo python setup.py install
sudo cp ~/python-controler/setup/src/geckodriver /usr/bin/geckodriver
sudo chmod +x /usr/bin/geckodriver
hostname -I
sudo cp -r ~/python-controler/ssh/* /home/pi/Desktop/
sudo chmod 755 /home/pi/Desktop/*.sh
echo "success"
sleep 10




