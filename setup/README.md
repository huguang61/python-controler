# 生产系统配置和开发说明文档

## 文件目录

* python-controler

  * backend「后台模块」

    * apiback.php

  * dist「生产系统4.0前端模块」

    * static
      * css
      * js
      * Img
    * index.html

  * setup「环境配置模块」

    * src 「环境配置模块核心依赖文件」
    * setup.sh 「生产系统环境配置脚本」

  * src「核心源码模块」

    * comm「旧方式，采用函数式编程的公共模块」
    * modsys4 「生产系统4.0方式的执行层」
    * system4.0.py「生产系统4.0执行层和通信层的主入口」
    * throughput-4.1.py「旧方式生产4.0过度测试的主入口」

  * ssh「桌面启动模块」

    * throughput-4.1.sh
    * system4.sh

  * View「前端显示模块」

    * static
    * throughput.html

## 系统开发说明

**注意**：此份说明主要是针对生产系统4.0的开发和维护进行详尽的说明，旧方式予以放弃不再维护处理。但是仍兼容和维护过渡方式。

### 部署说明

安装系统后，设置显示和键盘选项,打开ssh登录，最后设置下用户密码「passwd」为goodlife「为兼容旧系统的ssh登录使用」

使用root权限，创建ssh：

```
sudo su
ssh-keygen
cd .ssh
ssh-copy-id -i id_rsa.pub mike@192.168.16.17
```

按照安装过程所给的提示操作即可。

### 开发说明

在这里主要是指src这个文件夹。这个是整个系统运行的核心源码文件。而源码根目录文件夹下，又分为旧方式「不带有控制层和执行层的通信」和新方式「包含有通信层」分别两套机制进行说明。**注意**：旧方式和新方式所有的借口和使用方式一模一样。只是底层的原理不一样而已。

* pcba测试开发

  * 步骤说明

    * 登录 - - > 检测是否校准  - - > 检测是否进行pcba检测 - - > 检测sd卡 - - > 检测reset键 - - > 检测mesh键 - - > 检测led灯 - - > 检测拨动开关 - - > 检测usb - - > pcba检测完成

  * 接口函数说明

    * 登录

      ```python
      # 实例代码
      class Action(object):
        pass
      action = Action()
      action.device_type = device_type
      action.step = 'first'
      config.device = Device(action)
      ```

      **说明**：在进行生成实例之前我们要确定设备和树莓派已经进行连接，并且可以ping通.再pcba测试中接受两个参数device_type「设备的型号」和step「步骤」

    * 校准

      ```python
      # 实例代码
      config.device.calibration()
      ```

      **说明**：在登陆步骤进行实例化后，不需要传参数直接调用接口即可.测试成功返回True ,测试失败返回False,下面所有的接口都是如此

    * pcba检测

      ```python
      # 实例代码
      config.device.checkfirst()
      ```

    * sd卡

      ```python
      # 实例代码
      config.device.testsd()
      ```

    * reset键

      ```python
      # 实例代码
      config.device.reset()
      ```

    * mesh键

      ```python
      # 实例代码
      config.device.meshset()
      ```

    * led

      ```python
      # 实例代码
      config.device.testled()
      ```

    * 拨动开关

      ```Python
      # 实例代码
      config.device.testswitch()
      ```

    * usb

      ```Python
      # 实例代码
      config.device.testusb()
      ```

    * 完成

      ```Python
      # 实例代码
      config.device.firstok()
      ```

* product测试开发

  * 步骤说明

    * 登录 - - > 检测是否校准 - - > 检测是否进行pcba检测 - - > 检测sd卡 - - > 检测reset键 - -> 检测mesh键 - - > 检测拨动开关 - - > product检测完成

  * 接口函数说明「在这部分中和pcba重复的接口函数不再重复赘述」

    * 登录

      ```python
      # 实例代码
      class Action(object):
        pass
      action = Action()
      action.device_type = device_type
      action.step = 'second'
      action.mac_address = info[0]
      action.sn_now_use = info[2]
      action.sn_back_up = info[3]
      action.ddns_name = info[1]
      ​```
      旧方式需要多传三个参数
      ​```
      config.device = Device(action)
      ```

    * 测试完成

      ```python
      # 实例代码
      config.device.secondok()
      ```

* throughput测试开发

  * 步骤说明

    * 登录 - - > 连接wifi - - > 检测mac地址 - - > 测试吞吐量rx - - > 测试吞吐量tx - - > throughput检测完成 

  * 接口函数说明

    * 登录

      ```python
      # 实例代码
      class Action(object):
        pass
      action = Action()
      action.device_type = device_type
      action.step = 'third'
      config.device = Device(action)
      ```

      **注意**：由于吞吐量测试和其他测试不一样。所以在这里所指的登录是指测试者选完型号所需要做的工作，即生成实例。

    * 连接wifi

      ```python
      # 实例代码
      mac = "E4:95:xxxx"
      config.device.pretreat(mac)
      ```

      **注意**：在传入执行层之后，我们只关注mac地址，mac地址是用来设置ssid用和比较mac地址使用。

    * mac地址

      ```python
      # 实例代码
      config.device.maccompare()
      ```

    * 吞吐量rx

      ```python
      # 实例代码
      config.device.speed_rx()
      ```

      **注意**：这里如果测试成功会返回rx的值。如果测试失败会返回False

    * 吞吐量tx

      ```Python
      # 实例代码
      config.device.speed_tx()
      ```

* 通信层开发

  * 接口函数说明

    ```python
    # 实例代码
    # 接受控制端的测试重置按钮的命令
    @client.route("onreset")
    def handleOnResetAction(client, action):
      if config.device.reset() is False:
        data = demjson.encode({'status': 0, 'index': action.index})
        else:
          data = demjson.encode({'status': 1, 'index': action.index})
          actionReply = Action.buildReplyAction(action, "onreset",data)
          client.sendAction(actionReply)
    ```

    **注意**：在调用通信层接口时，我们只需要简单的使用@client.route('xxxx')「括号里时控制层和通信层所共有的命令」然后接下来定义个函数即可。在控制层中发送的信息都会在action中。在返回信息至控制层中一定是json格式的编码。然后调用Action.buildReplyAction(action,"XXXX",data)「这里的action一定要原样返回，然后加上我们定义的命令，和数据即可」


#### 使用说明

```python
methods = "old" ##使用的方式如果是测试系统4.0，请将这里改成new
test_suite = "third" ## 测试步骤pcba「first」,product「second」，throughput「third」
mac = "E4:95:6E:41:B3:CD" # 设备的mac地址信息
device_type = "GL-AR150" # 设备的型号
```

**注意** ：通过配置上述样例的信息，就可以进行测试代码。





